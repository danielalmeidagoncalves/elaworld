# Import flask and template operators
from flask import Flask, render_template, jsonify
from flask_socketio import SocketIO, emit
from threading import Lock
import bitalino
import random
import time
import os
import numpy


# Define the WSGI application object
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'

# Configurations
async_mode = None
app.config.from_object('config')
socketio = SocketIO(app, async_mode=async_mode)
socketio.init_app(app, cors_allowed_origins="*")
thread = None
thread_lock = Lock()


@app.route('/', methods=['GET'])
def root():
    return render_template('index.html')


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    res = {}
    return jsonify(results=res), 404


@socketio.on('event', namespace='/test')
def test_message(message):
    emit('response', {'data': message['data']})


@socketio.on('broadcast', namespace='/test')
def test_message(message):
    emit('response', {'data': message['data']}, broadcast=True)


@socketio.on('connect', namespace='/test')
def test_connect():
    print("SOCKET CONNECT")
    emit('response', {'data': 'READY'})
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(start_background_connection_handler)

    emit('response', {'data': 'READY'})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


def start_background_connection_handler():
    is_in_demo = os.environ.get('demo')
    print(is_in_demo)
    if is_in_demo == "1":
        handle_demo_mode()
    else:
        handle_bitalino_connection()


def handle_bitalino_connection():
    macAddress = "/dev/tty.BITalino"
    device = bitalino.BITalino(macAddress, 10)
    acquisitionTime = 5
    acqChannels = [0]
    samplingRate = 10
    threshold = 50
    acquireLoop = 0
    nFrames = 100

    # running_time = 5
    # batteryThreshold = 39


    # nSamples = 1

    device.start(samplingRate, acqChannels)
    print("Starting getting signal from Bitalino")

    while acquireLoop != (acquisitionTime * samplingRate):
        dataAcquired = device.read(nFrames)
        print(dataAcquired)
        EMG = dataAcquired[1, :]
        print(EMG)
        # center the EMG signal baseline at zero, by subtracting its mean
        # calculate the mean value of the absolute of the EMG signal
        value = numpy.mean(abs(EMG - numpy.mean(EMG)))
        if value >= threshold:
            # turn digital ports on
            # device.trigger([1, 1, 1, 1])
            print("threshold hit, on the value: ", value)
            socketio.emit('response', {'data': 'CALL'}, namespace='/test')
        # else:
            # turn digital ports off
            # device.trigger([0, 0, 0, 0])
        acquireLoop += nFrames

    device.stop()
    device.close()
    print("Stopping getting signal")

    # start = time.time()
    # end = time.time()
    # while (end - start) < running_time:
    #     # Read samples
    #     print(device.read(nSamples))
    #     end = time.time()
    # device.battery(batteryThreshold)
    # print(device.version())


def handle_demo_mode():
    status = 'READY'
    while True:
        socketio.sleep(10)
        mocked_connection = random.randint(0, 1)
        if mocked_connection == 0:
            if status != 'CALL':
                status = 'CALL'
                socketio.emit('response', {'data': status}, namespace='/test')
        else:
            if status != 'CONNECTED':
                status = 'CONNECTED'
                socketio.emit('response', {'data': status}, namespace='/test')

