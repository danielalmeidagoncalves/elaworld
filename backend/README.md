# elaworld

Economic adapted bell for ALS (AMYOTROPHIC LATERAL SCLEROSIS)

## Event server

The event server is a flask application structure following [Digital Ocean's How To Structure Large Flask Applications Tutorial](https://www.digitalocean.com/community/tutorials/how-to-structure-large-flask-applications).

**Note:** the application was bootstrapped without support for forms or database access.

This setup uses [virtualenv](https://docs.python-guide.org/dev/virtualenvs/#lower-level-virtualenv) to deal with local dependency management.


### Dependency management using pip

**Note** don't forget to activate your virtualenv or use `env/bin/pip`

Install dependencies by running:
```bash
pip install -r requirements.txt
```

Update dependencies by running:
```bash
pip freeze > requirements.txt
```

### Running the application

```bash
python run.py
```
