# frontend

One can either use npm or yarn package manager

## Project setup
```
npm install | yarn
```

### Compiles and hot-reloads for development
```
npm run serve | yarn run server
```

### Compiles and minifies for production
```
npm run build | yarn run build
```

### Run your unit tests
```
npm run test:unit | yarn run test:unit
```

### Lints and fixes files
```
npm run lint | yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
