module.exports = {
    configureWebpack: {
        devtool: 'source-map'
    },
    devServer: {
        host: "localhost"
    },
    css: {
      extract: true,
      loaderOptions: {
        scss: {
            prependData: `@import "~@/styles/_globals.scss";`
        }
      }
    }
  };