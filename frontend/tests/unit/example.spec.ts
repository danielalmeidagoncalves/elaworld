import { shallowMount } from '@vue/test-utils';
import Elaworld from '@/components/Elaworld.vue';

describe('Elaworld.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallowMount(Elaworld, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
