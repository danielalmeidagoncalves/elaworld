import EventType from "@/entities/eventtype.enum";

interface DeviceEvent {
    type: EventType;
    timestamp: number;
}

export default DeviceEvent;