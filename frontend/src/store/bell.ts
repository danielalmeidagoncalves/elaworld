import Vue from "vue";
import DeviceEvent from "./deviceevent";
import EventType from '@/entities/eventtype.enum';

export const Store = Vue.observable({
  bell: {
    events: [] as DeviceEvent[]
  }
});

export const Mutations = {
  add(event: EventType, timestamp: number) {
    const deviceEvent: DeviceEvent = {
        type: event,
        timestamp
    }
    Store.bell.events.push(deviceEvent);
  }
};
